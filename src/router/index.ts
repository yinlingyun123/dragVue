import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import IndexPage from "@/views/NotePage.vue";
import Echarts3D from "@/views/Echarts3D.vue";
import Map3D from "@/views/Map3D.vue";
import ThreeJs from "@/views/ThreeJs.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "IndexPage",
    component: IndexPage,
  },
  {
    path: "/Echarts3D",
    name: "Echarts3D",
    component: Echarts3D,
  },
  {
    path: "/Map3D",
    name: "Map3D",
    component: Map3D,
  },
  {
    path: "/ThreeJs",
    name: "ThreeJs",
    component: ThreeJs,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
