// 拖拽的指令
const drag = {
  mounted(el: any, binding: any) {
    // 自定义属性，判断是否可拖拽
    if (!binding.value || !el) return;

    // 触发拖动的dom节点
    const domDrag = el.querySelector(".dom_drag");

    // 拖动所在的容器
    const parentNode = el.parentNode || el.parentElement;

    parentNode.style.cssText += ";position:relative;";
    el.style.cssText += ";position:fixed;";

    domDrag.onmousedown = (e: any) => {
      // 鼠标按下，计算当前元素距离可视区的距离
      const disX = e.clientX - el.offsetLeft;
      const disY = e.clientY - el.offsetTop;

      const dragDomWidth = el.clientWidth; // 对话框宽度
      const dragDomHeight = el.clientHeight; // 对话框高度

      const minDragDomLeft = 0;
      const maxDragDomLeft = parentNode.clientWidth - dragDomWidth;

      const minDragDomTop = 0;
      const maxDragDomTop = parentNode.clientHeight - dragDomHeight;

      document.onmousemove = function (event) {
        // 通过事件委托，计算移动的距离
        let left = event.clientX - disX;
        let top = event.clientY - disY;

        // 边界处理
        if (left < minDragDomLeft) {
          left = minDragDomLeft;
        } else if (left > maxDragDomLeft) {
          left = maxDragDomLeft;
        }

        if (top < minDragDomTop) {
          top = minDragDomTop;
        } else if (top > maxDragDomTop) {
          top = maxDragDomTop;
        }

        // 移动当前元素
        el.style.cssText += `;left:${left}px;top:${top}px;`;
      };

      document.onmouseup = function () {
        document.onmousemove = null;
        document.onmouseup = null;
      };
    };

    domDrag.onmouseover = () => {
      document.onmousemove = null;
      document.onmouseup = null;
    };
  },
};
// 挂载，注册
const exportDrag = {
  install: function (app: any) {
    app.directive("dragFree", drag);
  },
};
export default exportDrag;
