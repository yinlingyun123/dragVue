const calculateLines: (data: any[]) => null | any[] = (data: any[]) => {
  if (!Array.isArray(data) || data.length < 2) {
    return null;
  }
  const list: any[] = [];
  for (let i = 1; i < data.length; i++) {
    list.push([data[0], data[i]]);
  }
  data.shift();
  return list.concat(calculateLines(data) || []);
};

export const mockData = (data: any) => {
  const list: any[] = Object.keys(data);
  return calculateLines(
    list.map((keys: string) => {
      if (!Array.isArray(data[keys]) || data[keys].length < 1) {
        throw "坐标请用数组表示";
      }
      return data[keys];
    })
  );
};
