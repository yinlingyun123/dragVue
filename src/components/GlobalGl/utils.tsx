interface TypeLine {
  coords: Array<Array<number>>;
  // 数据值
  value?: number;
  // 数据名
  name?: string;
  // 线条样式
  lineStyle?: Record<any, any>;
  // 点样式
  spotStyle?: Record<any, any>;
}

const linesData = (data: any) => {
  const lines: Array<any> = [];
  data.forEach((item: any) => {
    const temLine: Record<any, any> = {};
    temLine["coords"] = item.coords || item;
    if (item.value) {
      temLine["value"] = item.value;
    }
    if (item.name) {
      temLine["name"] = item.name;
    }
    if (item.lineStyle) {
      temLine["lineStyle"] = item.lineStyle;
    }
    if (item.spotStyle) {
      temLine["spotStyle"] = item.spotStyle;
    }
    lines.push(temLine);
  });
  return lines;
};

const spotsData = (data: any) => {
  return data.reduce((pre: Array<any>, current: Array<any>) => {
    return pre.concat(current);
  }, []);
};

const seriesCombine = (
  seriesLine: Record<any, any>,
  seriesScatter: Record<any, any>,
  data: Array<TypeLine | Array<Array<number>>>
) => {
  if (seriesLine.type !== "lines3D" || seriesScatter.type !== "scatter3D") {
    throw "series类型输入错误，请检查";
  }
  if (!data || data.length < 1) {
    return null;
  }
  seriesLine["data"] = linesData(data);
  seriesScatter["data"] = spotsData(data);
  return [seriesLine, seriesScatter];
};

export default {
  seriesCombine,
};
