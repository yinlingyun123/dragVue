import spotCombine from "./utils";
// 配置项和数据
const series = [
  {
    type: "lines3D",
    name: "lines3D",
    effect: {
      show: true,
      trailWidth: 2,
      trailLength: 2,
      trailOpacity: 1,
      trailColor: "rgb(30, 30, 60)",
    },

    lineStyle: {
      width: 3,
      color: "rgba(50, 50, 150,0)",
      // color: 'rgb(118, 233, 241)',
      opacity: 0,
    },
    blendMode: "lighter",
    data: [],
  },
  {
    type: "scatter3D",
    coordinateSystem: "globe",
    blendMode: "lighter",
    symbolSize: 2,
    itemStyle: {
      color: "rgb(50, 50, 150)",
      opacity: 0.2,
    },
    data: [],
  },
];

export default {
  globe: {
    // 基底图
    baseTexture: require("./images/earth1.png"),
    // 地形图
    heightTexture: require("./images/earth2.png"),
    displacementScale: 0.02,
    displacementQuality: "high",
    // 地球中三维图形的着色效果
    shading: "lambert",
    environment: "#000",
    // 真实感材质相关的配置项
    realisticMaterial: {
      roughness: 0.2,
    },
    // 后处理特效的相关配置
    postEffect: {
      enable: true,
    },
    temporalSuperSampling: {
      enable: true,
    },
    light: {
      ambient: {
        intensity: 0,
      },
      main: {
        intensity: 0.1,
        shadow: false,
      },
      ambientCubemap: {
        texture: "pisa.hdr",
        exposure: 1,
        diffuseIntensity: 0.5,
        specularIntensity: 2,
      },
    },
    // 用于鼠标的旋转，缩放等视角控制
    viewControl: {
      autoRotate: true,
      // 禁止缩放
      zoomSensitivity: 0,
    },
  },
  series: series,
};
