import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Drag from "@/utils/drag";
// 引入vue-bmap-gl
import VueBMap, { initBMapApiLoader } from "vue-bmap-gl";
import "vue-bmap-gl/dist/style.css";

// 初始化vue-bmap-gl
initBMapApiLoader({
  // 高德的key
  ak: "ugd28789UXGI57IinjgPLZTy6WLz4XwM",
});

const app = createApp(App);

app.use(store);
app.use(router);
app.use(Drag);
app.use(VueBMap);

app.mount("#app");
